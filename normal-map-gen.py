
import argparse
from PIL import Image
from math import sqrt

parser = argparse.ArgumentParser()
parser.add_argument("out",    help="Output file")
parser.add_argument("left",   help="Source image lit from left",   type=argparse.FileType("rb"))
parser.add_argument("right",  help="Source image lit from right",  type=argparse.FileType("rb"))
parser.add_argument("top",    help="Source image lit from top",    type=argparse.FileType("rb"))
parser.add_argument("bottom", help="Source image lit from bottom", type=argparse.FileType("rb"))

args = parser.parse_args()

left   = Image.open(args.left).convert("L")
right  = Image.open(args.right).convert("L")
top    = Image.open(args.top).convert("L")
bottom = Image.open(args.bottom).convert("L")

size = left.size
for i in (right, top, bottom):
	if size != i.size:
		raise ValueError("Image sizes do not match.")

out = Image.new("RGB", size)

for y in range(size[1]):
	for x in range(size[0]):
		coord = (x,y)
		
		# To get x/y components, add the positive side and subtract the negative side. We also convert the result from (-255,255) range to (-1,1)
		nx = (right.getpixel(coord) - left.getpixel(coord)) / 255
		ny = (top.getpixel(coord) - bottom.getpixel(coord)) / 255
		
		# To get the z component, just average the values.
		nz = sum((left.getpixel(coord), right.getpixel(coord), top.getpixel(coord), bottom.getpixel(coord))) / 4 / 255
		
		# Normalize the result
		l2 = nx*nx + ny*ny + nz*nz
		l = sqrt(l2)
		
		if l2 > 0.01:
			nx /= l
			ny /= l
			nz /= l
			
			# Convert from (-1,1) range to (0,255), or from (0,1) to (0,255)
			nx = round((nx*0.5+0.5)*255)
			ny = round((ny*0.5+0.5)*255)
			nz = round(nz*255)
			
			out.putpixel(coord, (nx,ny,nz))
		else:
			out.putpixel(coord, (0,0,0))

out.save(args.out)
