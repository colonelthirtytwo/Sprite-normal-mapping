
local gl, glc, glu, glfw, glext = require("glfw").libraries()
local ffi = require "ffi"

local new_string = ffi.typeof("char[?]")
local src_ptr = ffi.new("const char*[1]")
local int_buffer = ffi.new("int[1]")

local function getLog(shader)
	glext.glGetObjectParameterivARB(shader, glc.GL_OBJECT_INFO_LOG_LENGTH_ARB, int_buffer)
	if int_buffer[0] ~= 0 then
		local log_buffer = new_string(int_buffer[0])
		glext.glGetInfoLogARB(shader, int_buffer[0], int_buffer, log_buffer)
		return ffi.string(log_buffer, int_buffer[0])
	end
end

local Shader = {}

function Shader.new(srctbl, validate)
	-- Allocate values
	local program = glext.glCreateProgramObjectARB()
	local shaders = {}
	
	-- Compile each shader
	for typ, code in pairs(srctbl) do
		local shader = glext.glCreateShaderObjectARB(typ)
		shaders[typ] = shader
		
		src_ptr[0] = code
		int_buffer[0] = #code
		glext.glShaderSourceARB(shader, 1, src_ptr, int_buffer)
		glext.glCompileShaderARB(shader)
		
		glext.glGetObjectParameterivARB(shader, glc.GL_OBJECT_COMPILE_STATUS_ARB, int_buffer)
		
		if int_buffer[0] == 0 then
			error(getLog(shader), 2)
		end
		
		glext.glAttachObjectARB(program, shader)
	end
	
	-- Link and check
	glext.glLinkProgram(program)
	
	glext.glGetObjectParameterivARB(program, glc.GL_OBJECT_LINK_STATUS_ARB, int_buffer)
	if int_buffer[0] == 0 then
		error(getLog(program), 2)
	end
	
	-- Validate
	if validate then
		glext.glValidateProgramARB(program)
		print(getLog(program))
	end
	
	return program, shaders
end


return Shader
