
math.randomseed(os.time())

local ffi = require "ffi"
local lj_glfw = require "glfw"
local bit = require "bit"
local gl, glc, glu, glfw, glext = lj_glfw.libraries()

local SCREEN_W, SCREEN_H

lj_glfw.init()

local window = lj_glfw.Window(500, 500, "Normal Map")

function checkError()
	local errcode = gl.glGetError()
	if errcode ~= glc.GL_NO_ERROR then
		error("OpenGL Error: "..ffi.string(glu.gluErrorString(errcode)),0)
	end
end
--debug.sethook(checkError, "l")

SCREEN_W, SCREEN_H = window:getFramebufferSize()
window:makeContextCurrent()

lj_glfw.swapInterval(1)

gl.glEnable(glc.GL_TEXTURE_2D)

-- Load image
local diffuse_tex, normal_tex
do
	local freeimage = require "freeimage"
	
	-- Generate texture IDs
	local texture_ids = ffi.new("unsigned int[2]")
	gl.glGenTextures(2, texture_ids)
	
	diffuse_tex, normal_tex = texture_ids[0], texture_ids[1]
	
	-- Upload diffuse texture
	local diffuse_img = freeimage.FreeImage_Load(freeimage.FIF_PNG, "diffuse.png", 0)
	
	glext.glActiveTextureARB(glc.GL_TEXTURE0)
	gl.glBindTexture(glc.GL_TEXTURE_2D, diffuse_tex)
	gl.glTexParameteri(glc.GL_TEXTURE_2D, glc.GL_TEXTURE_MIN_FILTER, glc.GL_NEAREST)
	gl.glTexParameteri(glc.GL_TEXTURE_2D, glc.GL_TEXTURE_MAG_FILTER, glc.GL_NEAREST)
	gl.glTexImage2D(glc.GL_TEXTURE_2D, 0, glc.GL_RGBA,
		freeimage.FreeImage_GetWidth(diffuse_img),
		freeimage.FreeImage_GetHeight(diffuse_img),
		0,
		glc.GL_RGBA,
		glc.GL_UNSIGNED_BYTE,
		freeimage.FreeImage_GetBits(diffuse_img)
	)
	checkError()
	freeimage.FreeImage_Unload(diffuse_img)
	diffuse_img = nil
	
	-- Upload normal texture
	local normal_img = freeimage.FreeImage_Load(freeimage.FIF_PNG, "normal.png", 0)
	
	glext.glActiveTextureARB(glc.GL_TEXTURE0+1)
	gl.glBindTexture(glc.GL_TEXTURE_2D, normal_tex)
	gl.glTexParameteri(glc.GL_TEXTURE_2D, glc.GL_TEXTURE_MIN_FILTER, glc.GL_NEAREST)
	gl.glTexParameteri(glc.GL_TEXTURE_2D, glc.GL_TEXTURE_MAG_FILTER, glc.GL_NEAREST)
	gl.glTexImage2D(glc.GL_TEXTURE_2D, 0, glc.GL_RGB,
		freeimage.FreeImage_GetWidth(normal_img),
		freeimage.FreeImage_GetHeight(normal_img),
		0,
		glc.GL_RGB,
		glc.GL_UNSIGNED_BYTE,
		freeimage.FreeImage_GetBits(normal_img)
	)
	freeimage.FreeImage_Unload(normal_img)
	normal_img = nil
end

-- Load shader
local shader_program, shader_light
do
	local Shader = require "shader"
	
	local shaders
	shader_program, shaders = Shader.new({
		[glc.GL_VERTEX_SHADER_ARB] = [[
varying vec2 texcoord;
varying vec4 pos;

void main()
{
	pos = gl_ModelViewMatrix * gl_Vertex;
	gl_Position = gl_ProjectionMatrix * (pos);
	texcoord = gl_MultiTexCoord0.xy;
}
]],
		[glc.GL_FRAGMENT_SHADER_ARB] = [[
uniform vec3 lighting_pos;
uniform sampler2D tex_diffuse;
uniform sampler2D tex_normal;

varying vec2 texcoord;
varying vec4 pos;

void main()
{
	vec4 diffuse = texture2D(tex_diffuse, texcoord);
	if(diffuse.a < 0.1f)
		discard;
	
	vec3 tnormal = vec3(texture2D(tex_normal, texcoord)).zyx;
	tnormal = tnormal * vec3(2,2,1) - vec3(1,1,0);
	
	vec3 pnormal = normalize(lighting_pos - vec3(pos));
	
	float amnt = dot(tnormal, pnormal);
	amnt = clamp(amnt*0.3+0.7, 0, 1);
	gl_FragColor = diffuse*vec4(amnt, amnt, amnt, 1);
	
	//gl_FragColor = diffuse;
	//gl_FragColor = vec4(vec3(1,1,1)*amnt, 1);
	//gl_FragColor = vec4(tnormal, 1);
}
]],
	}, true)
	
	glext.glUseProgramObjectARB(shader_program)
	glext.glUniform1i(glext.glGetUniformLocationARB(shader_program, "tex_diffuse"), 0)
	glext.glUniform1i(glext.glGetUniformLocationARB(shader_program, "tex_normal"), 1)
	shader_light = glext.glGetUniformLocationARB(shader_program, "lighting_pos")
end

checkError()

--glext.glUseProgramObjectARB(0)

gl.glMatrixMode(glc.GL_PROJECTION)
gl.glLoadIdentity()
gl.glOrtho(-SCREEN_W/2,SCREEN_W/2,-SCREEN_H/2,SCREEN_H/2,-20,20)

gl.glMatrixMode(glc.GL_MODELVIEW)

local light_x, light_y, light_z = SCREEN_W/2, SCREEN_H/2, 100

-- Main loop
while not window:shouldClose() do
	gl.glClear(bit.bor(glc.GL_COLOR_BUFFER_BIT, glc.GL_DEPTH_BUFFER_BIT))
	
	light_x, light_y = window:getCursorPos()
	light_x = light_x - SCREEN_W/2
	light_y = SCREEN_H/2 - light_y
	
	gl.glEnable(glc.GL_TEXTURE_2D)
	glext.glUseProgramObjectARB(shader_program)
	glext.glUniform3f(shader_light, light_x, light_y, light_z)
	
	gl.glColor3d(1,1,1)
	gl.glBegin(glc.GL_QUADS)
		gl.glTexCoord2d(0,0)
		gl.glVertex2d(-100,-100)
		gl.glTexCoord2d(1,0)
		gl.glVertex2d( 100,-100)
		gl.glTexCoord2d(1,1)
		gl.glVertex2d( 100, 100)
		gl.glTexCoord2d(0,1)
		gl.glVertex2d(-100,100)
	gl.glEnd()
	
	
	glext.glUseProgramObjectARB(0)
	gl.glDisable(glc.GL_TEXTURE_2D)
	
	gl.glColor3d(1,1,0)
	gl.glBegin(glc.GL_LINES)
		gl.glVertex2d(-light_x*200, -light_y*200)
		gl.glVertex2d(-light_x*300, -light_y*300)
	gl.glEnd()
	
	window:swapBuffers()
	lj_glfw.pollEvents()
end

window:destroy()
lj_glfw.terminate()
