
local ffi = require "ffi"

ffi.cdef [[

	static const int BMP_DEFAULT               = 0;
	static const int BMP_SAVE_RLE              = 1;
	static const int CUT_DEFAULT               = 0;
	static const int DDS_DEFAULT               = 0;
	static const int EXR_DEFAULT               = 0;
	static const int EXR_FLOAT                 = 0x0001;
	static const int EXR_NONE                  = 0x0002;
	static const int EXR_ZIP                   = 0x0004;
	static const int EXR_PIZ                   = 0x0008;
	static const int EXR_PXR24                 = 0x0010;
	static const int EXR_B44                   = 0x0020;
	static const int EXR_LC                    = 0x0040;
	static const int FAXG3_DEFAULT             = 0;
	static const int GIF_DEFAULT               = 0;
	static const int GIF_LOAD256               = 1;
	static const int GIF_PLAYBACK              = 2;
	static const int HDR_DEFAULT               = 0;
	static const int ICO_DEFAULT               = 0;
	static const int ICO_MAKEALPHA             = 1;
	static const int IFF_DEFAULT               = 0;
	static const int J2K_DEFAULT               = 0;
	static const int JP2_DEFAULT               = 0;
	static const int JPEG_DEFAULT              = 0;
	static const int JPEG_FAST                 = 0x0001;
	static const int JPEG_ACCURATE             = 0x0002;
	static const int JPEG_CMYK                 = 0x0004;
	static const int JPEG_EXIFROTATE           = 0x0008;
	static const int JPEG_GREYSCALE            = 0x0010;
	static const int JPEG_QUALITYSUPERB        = 0x80;
	static const int JPEG_QUALITYGOOD          = 0x0100;
	static const int JPEG_QUALITYNORMAL        = 0x0200;
	static const int JPEG_QUALITYAVERAGE       = 0x0400;
	static const int JPEG_QUALITYBAD           = 0x0800;
	static const int JPEG_PROGRESSIVE          = 0x2000;
	static const int JPEG_SUBSAMPLING_411      = 0x1000;
	static const int JPEG_SUBSAMPLING_420      = 0x4000;
	static const int JPEG_SUBSAMPLING_422      = 0x8000;
	static const int JPEG_SUBSAMPLING_444      = 0x10000;
	static const int JPEG_OPTIMIZE             = 0x20000;
	static const int JPEG_BASELINE             = 0x40000;
	static const int KOALA_DEFAULT             = 0;
	static const int LBM_DEFAULT               = 0;
	static const int MNG_DEFAULT               = 0;
	static const int PCD_DEFAULT               = 0;
	static const int PCD_BASE                  = 1;
	static const int PCD_BASEDIV4              = 2;
	static const int PCD_BASEDIV16             = 3;
	static const int PCX_DEFAULT               = 0;
	static const int PFM_DEFAULT               = 0;
	static const int PICT_DEFAULT              = 0;
	static const int PNG_DEFAULT               = 0;
	static const int PNG_IGNOREGAMMA           = 1;
	static const int PNG_Z_BEST_SPEED          = 0x0001;
	static const int PNG_Z_DEFAULT_COMPRESSION = 0x0006;
	static const int PNG_Z_BEST_COMPRESSION    = 0x0009;
	static const int PNG_Z_NO_COMPRESSION      = 0x0100;
	static const int PNG_INTERLACED            = 0x0200;
	static const int PNM_DEFAULT               = 0;
	static const int PNM_SAVE_RAW              = 0;
	static const int PNM_SAVE_ASCII            = 1;
	static const int PSD_DEFAULT               = 0;
	static const int PSD_CMYK                  = 1;
	static const int PSD_LAB                   = 2;
	static const int RAS_DEFAULT               = 0;
	static const int RAW_DEFAULT               = 0;
	static const int RAW_PREVIEW               = 1;
	static const int RAW_DISPLAY               = 2;
	static const int RAW_HALFSIZE              = 4;
	static const int SGI_DEFAULT               = 0;
	static const int TARGA_DEFAULT             = 0;
	static const int TARGA_LOAD_RGB888         = 1;
	static const int TARGA_SAVE_RLE            = 2;
	static const int TIFF_DEFAULT              = 0;
	static const int TIFF_CMYK                 = 0x0001;
	static const int TIFF_PACKBITS             = 0x0100;
	static const int TIFF_DEFLATE              = 0x0200;
	static const int TIFF_ADOBE_DEFLATE        = 0x0400;
	static const int TIFF_NONE                 = 0x0800;
	static const int TIFF_CCITTFAX3            = 0x1000;
	static const int TIFF_CCITTFAX4            = 0x2000;
	static const int TIFF_LZW                  = 0x4000;
	static const int TIFF_JPEG                 = 0x8000;
	static const int TIFF_LOGLUV               = 0x10000;
	static const int WBMP_DEFAULT              = 0;
	static const int XBM_DEFAULT               = 0;
	static const int XPM_DEFAULT               = 0;

	typedef enum FREE_IMAGE_FORMAT {
		FIF_UNKNOWN = -1,
		FIF_BMP     = 0,
		FIF_ICO     = 1,
		FIF_JPEG    = 2,
		FIF_JNG     = 3,
		FIF_KOALA   = 4,
		FIF_LBM     = 5,
		FIF_IFF     = FIF_LBM,
		FIF_MNG     = 6,
		FIF_PBM     = 7,
		FIF_PBMRAW  = 8,
		FIF_PCD     = 9,
		FIF_PCX     = 10,
		FIF_PGM     = 11,
		FIF_PGMRAW  = 12,
		FIF_PNG     = 13,
		FIF_PPM     = 14,
		FIF_PPMRAW  = 15,
		FIF_RAS     = 16,
		FIF_TARGA   = 17,
		FIF_TIFF    = 18,
		FIF_WBMP    = 19,
		FIF_PSD     = 20,
		FIF_CUT     = 21,
		FIF_XBM     = 22,
		FIF_XPM     = 23,
		FIF_DDS     = 24,
		FIF_GIF     = 25,
		FIF_HDR     = 26,
		FIF_FAXG3   = 27,
		FIF_SGI     = 28,
		FIF_EXR     = 29,
		FIF_J2K     = 30,
		FIF_JP2     = 31,
		FIF_PFM     = 32,
		FIF_PICT    = 33,
		FIF_RAW     = 34
	} FREE_IMAGE_FORMAT;
	
	typedef enum FREE_IMAGE_TYPE {
		FIT_UNKNOWN = 0,
		FIT_BITMAP  = 1,
		FIT_UINT16	= 2,
		FIT_INT16	= 3,
		FIT_UINT32	= 4,
		FIT_INT32	= 5,
		FIT_FLOAT	= 6,
		FIT_DOUBLE	= 7,
		FIT_COMPLEX	= 8,
		FIT_RGB16	= 9,
		FIT_RGBA16	= 10,
		FIT_RGBF	= 11,
		FIT_RGBAF	= 12,
	} FREE_IMAGE_TYPE;

	typedef struct FIBITMAP { void* data; } FIBITMAP;

	FIBITMAP * FreeImage_Load(FREE_IMAGE_FORMAT fif, const char *filename, int flags);
	void FreeImage_Unload(FIBITMAP *dib);
	uint8_t* FreeImage_GetBits(FIBITMAP *dib);
	unsigned FreeImage_GetBPP(FIBITMAP *dib);
	unsigned FreeImage_GetWidth(FIBITMAP *dib);
	unsigned FreeImage_GetHeight(FIBITMAP *dib);
	FIBITMAP* FreeImage_ConvertToType(FIBITMAP *src, FREE_IMAGE_TYPE dst_type, int32_t scale_linear);
]]

local freeimage = ffi.load "freeimage"

return freeimage
